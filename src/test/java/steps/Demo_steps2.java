package steps;

import io.cucumber.java.en.*;

public class Demo_steps2 {
	@Given("I want to write a step with precondition")
	public void i_want_to_write_a_step_with_precondition() {
	    System.out.println("1ere etape 2");
	}

	@Given("some other precondition")
	public void some_other_precondition() {
		System.out.println("2eme etape 2");
	}

	@When("I complete action")
	public void i_complete_action() {
		System.out.println("3eme etape 2");
	}

	@When("some other action")
	public void some_other_action() {
		System.out.println("4eme etape 2");
	}

	@When("yet another action")
	public void yet_another_action() {
		System.out.println("5eme etape 2");
	}

	@Then("I validate the outcomes")
	public void i_validate_the_outcomes() {
		System.out.println("6eme etape 2");
	}

	@Then("check more outcomes")
	public void check_more_outcomes() {
		System.out.println("7eme etape 2");
	}




}
