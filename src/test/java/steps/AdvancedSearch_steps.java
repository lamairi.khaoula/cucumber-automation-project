package steps;

import static org.junit.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;


public class AdvancedSearch_steps {
	WebDriver driver;
	@Given("Je suis sur la page Advanced Search")
	public void je_suis_sur_la_page_Advanced_Search() {
		WebDriverManager.firefoxdriver().setup();
	    driver=new FirefoxDriver();
	    driver.manage().window().maximize();
	    driver.get("https://www.ebay.ca/sch/ebayadvsearch");
		
	}
	@When("Je clique sur le logo")
	public void je_clique_sur_le_logo() {
	   driver.findElement(By.id("gh-la")).click();
	}

	@Then("Je retourne a la page ebayHome")
	public void je_retourne_a_la_page_ebayHome() throws Exception {
		String expectedUrl="https://www.ebay.ca/";
		   String expectedTitle="Electronics, Cars, Fashion, Collectibles & More | eBay";
		   String urlObtenu=driver.getCurrentUrl();
		   String titleObtenu=driver.getTitle();
		   if(!urlObtenu.equalsIgnoreCase(expectedUrl))
			   fail("URL incorrect");
		   if(!titleObtenu.equalsIgnoreCase(expectedTitle))
			   fail("Title non attendu");
		   Thread.sleep(4000);
		   driver.quit();
	    
	}


}
