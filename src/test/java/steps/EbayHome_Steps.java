package steps;

import static org.junit.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;


public class EbayHome_Steps {
	WebDriver driver;

	@Given("Je suis sur la page accueil de ebay")
	public void je_suis_sur_la_page_accueil_de_ebay() {
	    WebDriverManager.firefoxdriver().setup();
	    driver=new FirefoxDriver();
	    driver.manage().window().maximize();
	    driver.get("https://www.ebay.ca/");
	}

	@When("Je clique sur le lien recherche avancee")
	public void je_clique_sur_le_lien_recherche_avancee() {
	    driver.findElement(By.id("gh-as-a")).click();
	}

	@Then("Je navigue vers la page de recherche avancee")
	public void je_navigue_vers_la_page_de_recherche_avancee() throws Exception {
	   String expectedUrl="https://www.ebay.ca/sch/ebayadvsearch";
	   String expectedTitle="eBay Search: Advanced Search";
	   String urlObtenu=driver.getCurrentUrl();
	   String titleObtenu=driver.getTitle();
	   if(!urlObtenu.equalsIgnoreCase(expectedUrl))
		   fail("URL incorrect");
	   if(!titleObtenu.equalsIgnoreCase(expectedTitle))
		   fail("Title non attendu");
	   Thread.sleep(4000);
	   driver.quit();
		   
	}
	//sc�nario2: recherche d'un article
	@When("Je cherche un laptop")
	public void je_cherche_un_laptop() {
	    driver.findElement(By.id("gh-ac")).sendKeys("Laptop");
	    driver.findElement(By.id("gh-btn")).click();
	}

	@Then("Je valide que au moins {int} resultats sont affiches")
	public void je_valide_que_au_moins_resultats_sont_affiches(Integer int1) {
	    
	}

	
	
	
}
