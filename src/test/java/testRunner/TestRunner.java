package testRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features= {"features"}, //chemin de dossier features
		glue= {"steps"},		// chemin du dossier contenant les steps definitions
		dryRun= false,			// true: permet d'executer les features qui n'ont pas de glue code
		monochrome=true,		//formater l'affichage
		tags="@sc2",		// execute tag1 ou testerlogin+ ou les deux si il existe les deux
		//name="Title",      		// cette option permet d'executer tous les senarios qui contiennent le mot title 
		plugin= {"pretty","json:target/json-report/cucumber.json"}
	
		)
public class TestRunner {

}
