Feature: Les fonctionnalités de ebay Homepage

  @sc1
  Scenario: Scenario qui teste le lien de la recherche avancee
    Given Je suis sur la page accueil de ebay
    When Je clique sur le lien recherche avancee
    Then Je navigue vers la page de recherche avancee

  @sc2
  Scenario: Scenario qui teste la fonctionnalité recherche et vérifie le nombre article trouvés
    Given Je suis sur la page accueil de ebay
    When Je cherche un laptop
    Then Je valide que au moins 1000 resultats sont affiches
